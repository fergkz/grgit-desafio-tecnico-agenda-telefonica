"use strict"; 
 
// Include gulp 
var gulp = require('gulp'); 
var concat = require('gulp-concat'); 
var uglify = require('gulp-uglify'); 
var autowatch = require('gulp-autowatch'); 
var minify = require('gulp-minify'); 
var cleanCSS = require('gulp-clean-css');
var sass = require('gulp-sass');
var stripCssComments = require('gulp-strip-css-comments');

gulp.task('javascript', function() {
    gulp.src([
        './assets/js/jquery-3.2.1.min.js',
        './assets/js/jquery.maskedinput.min.js',
        './assets/js/jquery.validate.min.js',
        './resources/javascript/page-home.js'
    ]).pipe(concat('home.min.js')).pipe(uglify())
      .pipe(gulp.dest('./assets/js/page/'));
});

gulp.task('sass', function () {
    gulp.src([
        './resources/scss/reset.scss',
        './assets/css/bootstrap.css',
        './resources/scss/frameset.scss',
        './resources/scss/page-home.scss'
    ]).pipe(sass().on('error', sass.logError))
      .pipe(concat('home.min.css'))
      .pipe(cleanCSS())
      .pipe(gulp.dest('./assets/css/page/'))
      .pipe(cleanCSS())
      .pipe(stripCssComments());
    
});

gulp.task('default', ['javascript','sass']);

gulp.watch('resources/scss/**/*.scss', ['sass']);
gulp.watch('resources/javascript/**/*.js', ['javascript']);