<?php

namespace Controllers;

use Models\PhonebookModel;
use Route;

class PhonebookController
{
    public static function listAll()
    {        
        $render = PhonebookModel::get();
        header('Content-Type: application/json');
        echo json_encode($render);
    }
    
    public static function insert()
    {
        $phonebookModel = new PhonebookModel();
        $phonebookModel->name = Route::input()->post['name'];
        $phonebookModel->number = Route::input()->post['phone'];
        $phonebookModel->insert();
    }
    
    public static function update()
    {
        $phonebookModel = new PhonebookModel();
        $phonebookModel->id = Route::input()->patch['id'];
        $phonebookModel->name = Route::input()->patch['name'];
        $phonebookModel->number = Route::input()->patch['phone'];
        $phonebookModel->update();
    }
    
    public static function delete()
    {
        $phonebookModel = new PhonebookModel();
        $phonebookModel->id = Route::input()->delete['id'];
        $phonebookModel->delete();
    }
}