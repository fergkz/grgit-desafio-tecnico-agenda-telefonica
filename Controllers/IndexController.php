<?php

namespace Controllers;

use Libraries\View;

class IndexController
{
    public static function index() {
        $view = new View('index.php');
        $view->minify();
        $view->show();
    }
}
