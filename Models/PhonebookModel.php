<?php

namespace Models;

use System\Database;

class PhonebookModel
{
    public $id = null;
    public $name = null;
    public $number = null;
    public $registeredDate = null;
    
    public static function get( $id = null )
    {
        $res = Database::query('select * from phonebook');
        
        $rows = [];
        while( $row = $res->fetchArray() ) {
            $phonebook = new PhonebookModel;
            $phonebook->id = $row['id'];
            $phonebook->name = $row['name'];
            $phonebook->number = $row['number'];
            $phonebook->registeredDate = $row['registeredDate'];
            $rows[] = $phonebook;
        }
        
        return $rows;
    }
    
    public function insert()
    {
        if( !$this->validateName() ) { die('Wrong name format'); }
        $this->registeredDate = date('Y-m-d H:i:s');
        Database::query('insert into phonebook (name, number, registeredDate) values (:name, :number, :registeredDate)', [
            'name' => $this->name,
            'number' => $this->number,
            'registeredDate' => $this->registeredDate
        ]);
    }
    
    public function update()
    {
        if( !$this->validateName() ) { die('Wrong name format'); }
        $res = Database::query('update phonebook set name = :name, number = :number where id = :id', [
            'name' => $this->name,
            'number' => $this->number,
            'id' => $this->id
        ]);
    }
    
    public function delete()
    {
        $res = Database::query('delete from phonebook where id = :id', [
            'id' => $this->id
        ]);
    }
    
    private function validateName() {
        $nome = trim($this->name);
        $regex  = "/^\D{2,} \D{2,}/";
        return preg_match($regex, $nome);
    }
}