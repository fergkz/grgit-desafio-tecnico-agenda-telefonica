<?php

ini_set('display_errors', E_ALL);

use Controllers\IndexController;
use Controllers\PhonebookController;

function debug( $var = '', $noDie = false ) { echo '<pre>'; var_dump($var); echo '</pre>'; if( !$noDie ){ exit; }  }

if( file_exists('env.php') ) {
    include 'env.php';
}

if( !defined('APPURL') ) {
    define('APPURL', 'http://app.fergkz.com/grgit-agenda');
}
define('APPPATH', dirname(__FILE__));
define('SQLITE_PATH', APPPATH . DIRECTORY_SEPARATOR . 'cache' . DIRECTORY_SEPARATOR . 'database.sqlite');

include 'System/autoloader.php';
include 'System/route.php';

Route::parse('', function(){ IndexController::index(); });
Route::parse('/ws/phonebook(.*)', function() {
    switch( Route::request()->method ){
        default:
        case 'GET':
            isset(Route::input()->get['q']) ? PhonebookController::search() : PhonebookController::listAll();
        break;
        case 'POST':
            PhonebookController::insert();
        break;
        case 'PATCH':
            PhonebookController::update();
        break;
        case 'DELETE':
            PhonebookController::delete();
        break;
    }
});