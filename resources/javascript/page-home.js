var Phonebook = Phonebook || {
    list: function() {
        $.ajax({
            url: $('body').attr('data-base-url') + '/ws/phonebook',
            type: 'get',
            dataType: 'json',
            success: function( data ) {
                $('#tbl_row_show').html('');
                $(data).each(function( ind, row ) {
                    var $copy = $($('#tbl_row_template').clone().html());
                    $copy.find('td.id').html( row.id );
                    $copy.find('td.name').html( row.name );
                    $copy.find('td.number').html( row.number );
                    $copy.find('td.since').html( row.registeredDate );
                    $copy.find('input.name').val( row.name );
                    $copy.find('input.phone').val( row.number );
                    $copy.find('input.id').val( row.id );
                    
                    $copy.find('.change').on('click', function(e){
                        e.preventDefault();
                        $copy.find('td.id, td.name, td.number, td.since, td.options').hide();
                        $copy.find('td.change-form').show();
                    });
                    
                    $copy.find('.delete').on('click', function(e){
                        e.preventDefault();
                        if( confirm('You really want delete the contact?') ) {
                            Phonebook.delete(row.id);
                        }
                    });
                    
                    $copy.find('.change-btn a').on('click', function(e){
                        e.preventDefault();
                        Phonebook.update($copy);
                    });
                    
                    $('#tbl_row_show').append( $copy );
                });
                Phonebook.helper.declareMasks();
            }
        });
    },
    search: function() {
        // get
    },
    insert: function() {
        if( Phonebook.helper.validate($('#form_register_new')) ) {
            $.ajax({
                url: $('body').attr('data-base-url') + '/ws/phonebook/',
                type: 'post',
                data: {
                    'id': null,
                    'name': $('#form_register_name').val(),
                    'phone': $('#form_register_phone').val()
                },
                dataType: 'json',
                complete: function() {
                    $('#form_register_name').val('');
                    $('#form_register_phone').val('');
                    Phonebook.list();
                }
            });
        }
    },
    update: function( $form ) {
        if( Phonebook.helper.validate($form.find('form')) ) {
            $.ajax({
                url: $('body').attr('data-base-url') + '/ws/phonebook/',
                type: 'PATCH',
                data: {
                    'id': $form.find('input.id').val(),
                    'name': $form.find('input.name').val(),
                    'phone': $form.find('input.phone').val()
                },
                dataType: 'json',
                complete: function() {
                    Phonebook.list();
                }
            });
        }
    },
    delete: function( id ) {
        $.ajax({
            url: $('body').attr('data-base-url') + '/ws/phonebook/',
            type: 'DELETE',
            data: { 'id': id },
            dataType: 'json',
            complete: function() {
                Phonebook.list();
            }
        });
    },
    helper: {
        validate: function( $el ) {
            $el.validate({
                rules: {
                    name: { required: true, real_name: true },
                    phone: { required: true }
                }
            });
            return $el.valid();
        },
        declareMasks: function() {
            $(".mask-phone").mask("(99) 9999-9999?9");
        },
        onlyNumbers: function( string ) {
            var numberPattern = /\d+/g;
            return string.toString().match( numberPattern );
        },
        validateFilters: {
            isRealName: function(nome) {
                return /^\D{2,} \D{2,}/.test(nome);
            }
        }
    }
};

jQuery(function($){
    $.validator.addMethod("real_name", function( name ){ return Phonebook.helper.validateFilters.isRealName(name); }, "Wrong name format");
    Phonebook.list();
});