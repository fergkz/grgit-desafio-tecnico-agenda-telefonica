<html lang="pt-br">
    <head>
        <title>GRGIT - Agenda Telefônica</title>
        
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="expires" content="0">
        
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Agenda Telefônica com persistência">
        <meta name="author" content="Fernando Gurkievicz <fergkz@gmail.com>">
        <meta name="keywords" content="HTML,CSS,XML,JavaScript">
        
        <link type="text/css" rel="stylesheet" href="<?php echo Route::url('assets/css/page/home.min.css') ?>" />
    </head>
    <body data-base-url="<?php echo APPURL ?>">
        
        <div class="container container-body">
            
            <div class="header clearfix text-center">
                <h3 class="text-muted text-center">DESAFIO TECNICO TI FULL STACK</h3>
            </div>
            
            <form class="row row-form" id="form_register_new">
                <label class="col-sm-12">Register a new contact</label>
                <div class="col-md-6 col-sm-12 col-xs-12"><input type="text" name="name" id="form_register_name" class="form-control" placeholder="Name" /></div>
                <div class="col-md-4 col-sm-12 col-xs-12"><input type="tel" name="phone" id="form_register_phone" placeholder="Phone" class="form-control mask-phone" /></div>
                <div class="col-md-2 col-sm-12 col-xs-12"><a href="javascript:void(0)" onclick="Phonebook.insert()" class="btn btn-primary form-control">Register</a></div>
            </form>
            
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Number</th>
                            <th>Since</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody id="tbl_row_template">
                        <tr>
                            <td class="id col-xs-12" class="id"></td>
                            <td class="name"></td>
                            <td class="number"></td>
                            <td class="since"></td>
                            <td class="options">
                                <a href="javascript:void(0)" class="change btn btn-primary btn-sm">Change</a> 
                                <a href="javascript:void(0)" class="delete btn btn-danger btn-sm">Delete</a>
                            </td>
                            
                            <td class="change-form" colspan="5">
                                <form>
                                    <input type="hidden" name="id" class="id" />
                                    <div class="row row-template-inlines">
                                        <div class="col-md-6 col-sm-12">
                                            <input type="text" name="name" class="name form-control form-control-sm" placeholder="Name" />
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <input type="tel" name="phone" class="phone form-control form-control-sm mask-phone" placeholder="Phone" />
                                        </div>
                                        <div class="col-md-2 col-sm-12 change-btn">
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm form-control">Change</a>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                        
                    </tbody>
                    <tbody id="tbl_row_show"></tbody>
                </table>
            </div>

        </div>
        
        <footer class="footer">
            <div class="container">
                <p>&copy; Fernando Gurkievicz - <a href="http://fergkz.com" target="_blank">fergkz.com</a> - <a href="mailto:fergkz@gmail.com">fergkz@gmail.com</a> - 2017</p>
            </div>
        </footer>
        
        <script type="text/javascript" src="<?php echo Route::url('assets/js/page/home.min.js') ?>"></script>
    </body>
</html>