<?php

spl_autoload_register(function($class_name) {
    $filename =  APPPATH . DIRECTORY_SEPARATOR . str_replace("\\", DIRECTORY_SEPARATOR, $class_name) . '.php';
    if( file_exists( $filename ) ) {
        include_once $filename;
    }
});