<?php

class Route
{
    public $input = null;
    public $request = null;
    
    public function __construct()
    {
        $this->request = Route::request();
        $this->input = Route::input($this);
    }
    
    public static function request()
    {
        $request = new stdClass();
        $request->method = $_SERVER['REQUEST_METHOD'];
        $defaultPath = @$_SERVER['ORIG_PATH_INFO'] ?: $_SERVER['PATH_INFO'] ?: $_SERVER['PATH_TRANSLATED'] ?: $_SERVER['PHP_SELF'];
        $defaultPost = (strpos($defaultPath, '/:::') > 0 ? strpos($defaultPath, ':::/') : 0) + 5;
        $request->url = substr($defaultPath, $defaultPost);
        return $request;
    }
    
    public static function input( Route $obj = null )
    {
        $route = $obj ? $obj : new Route();
        $input = new stdClass();
        
        $input->get = null;
        $input->post = null;
        $input->patch = null;
        $input->delete = null;
        
        $input->get = isset($_GET) && is_array($_GET) && count($_GET) > 0 ? $_GET : null;
        $input->post = isset($_POST) && is_array($_POST) && count($_POST) > 0 ? $_POST : null;
        
        switch ( $route->request->method ) {
            case 'PATCH':
                parse_str(file_get_contents('php://input'), $input->patch);
            break;
            case 'DELETE':
                parse_str(file_get_contents('php://input'), $input->delete);
            break;
        }
        
        return $input;
    }
    
    private function validUrl( $urlParse )
    {
        $clean_url = trim( $urlParse, "/");
        $preg = '/'.str_replace("/", "\/", ($clean_url ? $clean_url : "(^\s*$)")).'/mi';
        
        return ( !$preg && !$this->request->url )
            or ( $preg && preg_match( $preg, $this->request->url) );
    }
    
    public static function parse( $urlParse, $callback )
    {
        $route = new Route();
        if( $route->validUrl($urlParse) ) {
            $parsed = "";
            $clean_url = trim( $urlParse, "/");
            $preg = '/'.str_replace("/", "\/", ($clean_url ? $clean_url : "(^\s*$)")).'/mi';
            preg_match_all( $preg, $route->request->url, $parsed);
            $str_params = trim( $parsed[1][0], '/' );
            $params = explode("/", $str_params ? $str_params : "");
            return call_user_func_array($callback, $params);
        }
    }
    
    public static function url( $relative = '' ) {
        return rtrim(APPURL, '/') . '/' . $relative;
    }
}
