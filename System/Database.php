<?php

namespace System;

use SQLite3;

class Database
{
    private $db = null;
    
    public function __construct()
    {
        $this->db = new SQLite3(SQLITE_PATH);
        $this->prepareDatabase();
    }
    
    public function prepareDatabase()
    {
        $this->db->query("
            create table if not exists phonebook ( id INTEGER PRIMARY KEY autoincrement, name, number, registeredDate )
        ");
    }
    
    public static function query($query, $bind = [])
    {
        $database = new Database();
        
        $stmt = $database->db->prepare($query);
        foreach( $bind as $key => $value ) {
            $type = SQLITE3_TEXT;
            switch (gettype($value)) {
                case 'double': $type =  SQLITE3_FLOAT;
                case 'integer': $type =  SQLITE3_INTEGER;
                case 'boolean': $type =  SQLITE3_INTEGER;
                case 'NULL': $type =  SQLITE3_NULL;
                case 'string': $type =  SQLITE3_TEXT;
            }
            $stmt->bindValue(":{$key}", $value, $type);
        }
        $result = $stmt->execute();
        
        return $result;
    }
}