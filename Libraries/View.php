<?php

namespace Libraries;

class View
{
    private $filecontent = "";
    
    public function __construct( $view_filename = '' ){
        ob_start();
        include_once APPPATH . DIRECTORY_SEPARATOR . 'resources' . DIRECTORY_SEPARATOR . str_replace('/', DIRECTORY_SEPARATOR, $view_filename);
        $this->filecontent = ob_get_contents();
        ob_end_clean();
    }
    
    public function minify()
    {
        $replace = array(
            '/<!--[^\[](.*?)[^\]]-->/s' => '',
            "/\r/"                      => '',
            "/>\n</"                    => '><',
            "/>\s+\n</"    				=> '><',
            "/>\n\s+</"					=> '><',
            '/(\s)+/s' => ' ',         // shorten multiple whitespace sequences
        );
        
        $this->filecontent = preg_replace(array_keys($replace), array_values($replace), $this->filecontent);
    }
    
    public function show()
    {
        echo $this->filecontent;
    }
}